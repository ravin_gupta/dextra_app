var express = require('express');
var router = express.Router();

var redisHandler = require('../handlers/redis');
var googleMaps = require('../handlers/geocoding');

var userModel = require('../models/user');

var helper = require('../helper');

var {get_pagination} = helper;

/* GET all users data. */
// Query Params: 
// page: page number to fetch (default=1)
// per_page: limit of number of results (default=10)
// sort: [recent, oldest] (default=recent)
// status: [complete, unregistered] (default=null) Optional
// createdOn_***: replace *** with any query type ex: gte, lt, lte etc (Optional)
// updatedOn_***: replace *** with any query type ex: gte, lt, lte etc (Optional)
router.get('/', [get_pagination], (req, res, next) => {
  let params = req.query;
  let filter_obj = {};
  let sort_obj = {sort: ['createdOn', params.sort === 'oldest' ? 1 : -1]}
  if (params.status){
    filter_obj['status'] = params.status;
  }
  for (let param of Object.keys(params)){
    if (param.startsWith('createdOn') || param.startsWith('updatedOn')){
      if (filter_obj[param.split('_')[0]] === undefined || filter_obj[param.split('_')[0]] === null){
        filter_obj[param.split('_')[0]] = {};
      }
      filter_obj[param.split('_')[0]]['$'+param.split('_')[1]] = params[param];
    }
  }
  userModel.getUsers(filter_obj, sort_obj, req.offset, req.per_page).then(async (user_result) => {
    var res_data = [];
    for(let temp_data of user_result){
      let lng = temp_data.googleLocation.loc.coordinates[0];
      let lat = temp_data.googleLocation.loc.coordinates[1];
      var formattedAddress = '';
      if (lng !== undefined && lng !== null && lat !== undefined && lat !== null){
        // Trying to fetch formattedAddress from Redis 
        await redisHandler.getRedisKey(temp_data._id.toString()).then((response)=>{
          formattedAddress = response;
        }).catch((err)=>{
          // console.log(err);
          // Need to handle this properly
        })

        if (formattedAddress != '' && formattedAddress != null && formattedAddress != undefined){
          // Address Found in the Redis
        } else {
          // Making Google Maps API call to fetch formattedAddress
          await googleMaps.getAddress(lat+','+lng).then(async (response)=>{
            formattedAddress = response.json.results[0].formatted_address;
            await redisHandler.addRedisKey(temp_data._id.toString(), formattedAddress);
          }).catch((err)=>{
            // console.log(err);
            // Need to handle this properly
          })
        }
      } else {
        // No latlng found
        // console.log('No latlng found');
      }
      temp_data['googleLocation']['formattedAddress'] = formattedAddress;
      res_data.push(temp_data);
    }
    res.send(res_data);
  }).catch(function(err){
    // console.log(err);
    res.send([]);
  });
});

module.exports = router;