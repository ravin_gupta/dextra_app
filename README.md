# Dextra NodeJS Assignment

Dextra NodeJS App uses some projects to work properly:

* [node.js](http://nodejs.org/) - evented I/O for the backend
* [Express](http://expressjs.com/) - fast node.js network app framework
* [Mongoose](https://mongoosejs.com/) - elegant mongodb object modeling for node.js
* [@google/maps](https://developers.google.com/maps/documentation/geocoding/start) - Maps Api for reverse Geocoding
* [redis](https://redis.io/) - in-memory data structure store

### Installation

Dextra App requires [Node.js](https://nodejs.org/), [Redis](https://redis.io/), [MongoDB](https://www.mongodb.com) to run.

Install the dependencies

```sh
$ cd dextra_app
$ npm install
```

### Part 1
script.js file placed in the same folder is a fully independent script which takes users.json as a input and uploads all the data on a single MongoDB collection named 'User'.

In Future, we can integrate models and handlers to shorten the code written in the script.js file.

How to run the script?

```sh
$ cd dextra_app
$ node script.js
```

Note: This works after installing the dependencies as mentioned above in the Installation section.

### Part 2
It's a NodeJS and ExpressJS based server side app which provides API's access to fetch users data from MongoDB.

How to run the app? (You can change the `config.js` file if you want to according to your setup)

```sh
$ cd dextra_app
$ DEBUG=dextra-app:* npm start
```

##### Endpoints
` /users ` : API endpoint designed to fetch users from the database

We can pass on multiple parameters to the same endpoint depending upon the use cases.
Parameters List

| Parameter | Use Case | Values | Default | Optional |
| --------- | -------- | ------ | ------- | -------- |
| page | Page Number | Integer | 1 | false |
| per_page | Limit of number of results | Integer | 10 | false |
| sort | Sorting based on createdOn field | recent, oldest | recent | false |
| status | Filter based on status field | complete, unregistered | null | true |
| createdOn_** | filter based on createdOn field | new Date() | null | true |
| updatedOn_** | filter based on updatedOn field | new Date() | null | true |

` NOTE: ** should replaced by any kind of date filter like gte, lt, lte, gt. `

##### Few Examples showing the use cases
Showing some the use cases or examples to fetch users data

```sh
$ curl "http://localhost:3000/users?sort=older&page=1&per_page=20"
$ curl "http://localhost:3000/users?sort=recent&status=complete&createdOn_lt=2017-04-23"
$ curl "http://localhost:3000/users?sort=recent&status=unregistered&page=2&per_page=5"
$ curl "http://localhost:3000/users?createdOn_lt=2017-04-23&createdOn_gte=2017-01-01&updatedOn_gte=2017-03-01"
```