var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  status: {
    type: String,
    index: true
  },
  updatedOn: {
    type: Date,
    index: true
  },
  createdOn: {
    type: Date,
    index: true
  },
  googleLocation: {
    formattedAddress: {
      type: String
    },
    loc: {
      coordinates: {
        type: [Number]
      }
    }
  }
});

userSchema.pre('save', function(next) {
  if (!this.createdOn) this.createdOn = new Date;
  this.updatedOn = new Date;
  next();
});

var UserModel = mongoose.model('User', userSchema);

function addUser(doc_body) {
  return (new UserModel(doc_body)).save();
}

function getUsers(doc_body, sort_obj, offset, limit){
  if (limit != 0){
    return UserModel.find(doc_body).sort([sort_obj.sort]).skip(offset).limit(limit);
  }
  return UserModel.find(doc_body).sort([sort_obj.sort]);
}

exports.addUser = addUser;
exports.getUsers = getUsers;