var mongoose = require('mongoose');
const fs = require('fs');
mongoose.connect('mongodb://localhost:27017/dextra', {useNewUrlParser: true});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

// Handeling User Model
var userSchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  status: {
    type: String,
    index: true
  },
  updatedOn: {
    type: Date,
    index: true
  },
  createdOn: {
    type: Date,
    index: true
  },
  googleLocation: {
    formattedAddress: {
      type: String
    },
    loc: {
      coordinates: {
        type: [Number]
      }
    }
  }
});

userSchema.pre('save', function(next) {
  if (!this.createdOn) this.createdOn = new Date;
  this.updatedOn = new Date;
  next();
});

var UserModel = mongoose.model('User', userSchema);

function addUser(doc_body) {
  return (new UserModel(doc_body)).save();
}

let rawdata = fs.readFileSync('users.json');
let users = JSON.parse(rawdata);  

db.once('open', function() {
  let docs_count = users.length;
  console.log("Total Documents: " + docs_count);
  let docs_inserted = 0;
  let perc_calc = 10;
  let doc_window = (docs_count/perc_calc);
  let perc_done = 0;
  let start_date = new Date();
  for (var value of users) {
    /* this way we can store googleLocation in different format if we want to */
    // var {googleLocation, ...jsonObj} = value;
    // jsonObj['location'] = {
    //   type: 'Point',
    //   ...googleLocation['loc']
    // }

    addUser(value).then(function(user_result){
      docs_inserted++;
      if((docs_inserted%doc_window) === 0){
        perc_done += perc_calc;
        console.log("Percent Done: " + perc_done + "%");
      }
      if (perc_done === 100){
        console.log('Done Inserting Documents in ' + (Number.parseInt((new Date() - start_date) / 1000)) + 's');
        setTimeout(()=>{
          process.exit(0);
        }, 1000);
      }
    }).catch(function(err){
      console.log(err);
    });
  }
});