const config = require('../config');
var {MONGO_HOST, MONGO_PORT, MONGO_DB} = config;

var mongoose = require('mongoose');
mongoose.connect('mongodb://'+MONGO_HOST+':'+MONGO_PORT+'/'+MONGO_DB, {useNewUrlParser: true});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
  console.log('Connected to mongodb');
});