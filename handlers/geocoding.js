const config = require('../config');
var {GEOCODE_API_KEY} = config;

var googleMaps = require('@google/maps');

const googleMapsClient = googleMaps.createClient({
  key: GEOCODE_API_KEY,
  Promise: Promise
});

exports.getAddress = (latlng) => {
  return googleMapsClient.reverseGeocode({latlng: latlng}).asPromise();
}