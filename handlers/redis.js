var redis = require('redis');
const {promisify} = require('util');

var client = redis.createClient();

const getAsync = promisify(client.get).bind(client);

var conn = client.on('connect', function(){
  console.log('Redis client connected');
});

var conn_err = client.on('error', function(err){
  console.log('Something went wrong ' + err);
});

exports.addRedisKey = (key, value, expireInHours=1) => {
  return client.set(key, value, 'EX', 60 * 60 * expireInHours);
}

exports.getRedisKey = (key) => {
  return getAsync(key);
}