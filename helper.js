exports.get_pagination = (req, res, next) => {
  let params = req.query;
  let page = params.page || 1;
  let per_page = params.per_page || 10;
  let offset = (page - 1) * per_page;
  req.page = page;
  req.per_page = per_page;
  req.offset = offset;
  next();
}